vlocity.cardframework.registerModule.controller('myappHomeProductController', ['$scope', '$rootScope', '$location', function($scope, $rootScope, $location) {
        
        $scope.initImageURL = function() {
            if($rootScope.instanceUrl) {
                var org = $rootScope.instanceUrl.split(".")[0].split("--")[1].toLowerCase();
                var cs = $rootScope.instanceUrl.split(".")[1];
                $rootScope.imageURL = "https://" + org + "-sit2-verizonbusinessmarkets." + cs + ".force.com/AuthUsers";
            }
        };
        
        $scope.initImageURL();
        $scope.openOrderDetailsPopUpCheck = false;
        
        if(_.get($scope.records,'Response.serviceResponse.customersList[0].billAccountInfo.mtnDetailList[0].serviceInfo.serviceType')=== 'LCI' && _.get($scope.records,'Response.serviceResponse.customersList[0].billAccountInfo.mtnDetailList[0].serviceInfo.serviceStatCd')=== 'A'){
            // For existing user
            $scope.activeOrder = false;
    	}
    	else {
            // For Active user
            $scope.activeOrder = true;
            if($scope.records) {
                var obj =  _.get($scope.records,'Response.serviceResponse.customersList[0].billAccountInfo.masterOrderList');
                $scope.masterOrderNumber = $scope.records.Response.serviceResponse.customersList[0].billAccountInfo.masterOrderList[0].masterOrderNumber;
                $scope.scheduledDate = new Date($scope.records.Response.serviceResponse.customersList[0].billAccountInfo.masterOrderList[0].dueDate);
                $scope.AppointmentStartTime = $scope.records.Response.serviceResponse.customersList[0].billAccountInfo.masterOrderList[0].technicianDispatchInfo.AppointmentStartTime;
                $scope.newDate = new Date($scope.scheduledDate);
            }
    	}
        
        // Billing 
        
        try {
            $scope.lastBillPaidDate = new Date( _.get($scope.records,'Response.serviceResponse.customersList[0].billAccountInfo.paymentInfo.lastPaymentDate'));
    		//remaining
    		
            $scope.lastBillPaidAmount = _.get($scope.records,'Response.serviceResponse.customersList[0].billAccountInfo.paymentInfo.lastPaymentAmount');
    		//remaining
            var retcustvaluesduedate = _.get($scope.records,'Response.serviceResponse.customersList[0].billAccountInfo.paymentInfo.paymentDueDate');
    		if(retcustvaluesduedate){
    			$scope.currentBillDueDate = new Date(retcustvaluesduedate);
    		}
        }
        catch(e) {
            $scope.currentBillDueDate = "NA";
        }	    
        
        
    	if(_.get($scope.records,'Response.serviceResponse.customersList[0].billAccountInfo.billAccountBalanceInfo.currentCumulativeBalance') !== 0) {
    	    $scope.currentBillAmount =  _.get($scope.records,'Response.serviceResponse.customersList[0].billAccountInfo.billAccountBalanceInfo.currentCumulativeBalance');
    	    $scope.price = parseFloat($scope.currentBillAmount);
    	    if($scope.price <= 0){$scope.disableBtn = 1;}
    	        else{$scope.disableBtn = 0;}
    	}
    	else {
    	    $scope.currentBillAmount = 0;
    	}
        
        // Tickets
        
        $scope.assignedCases = 0;
        $scope.inProgressCases = 0;
        
        // Looping through the Obj and find Open and In Progress Cases and counts 
        angular.forEach($scope.records.caseCount, function(value, key) {
            if(value.Status === 'Open') {
                //$scope.assignedCases = value.caseCount;
                $scope.openCases = value.caseCount;
            }
            if(value.Status === 'In Progress') {
                $scope.inProgressCases = value.caseCount;
            }
            if(value.Status === 'Resolved') {
                $scope.resolvedCases = value.caseCount;
            }
            if(value.Status === 'Closed') {
                $scope.closedCases = value.caseCount;
            }
        });
        
        // $scope.openCases = $scope.openCases !== undefined ? $scope.openCases : 0;
        // $scope.inProgressCases = $scope.inProgressCases !== undefined ? $scope.inProgressCases : 0;
        $scope.openAndInprogressCases = ($scope.openCases !== undefined ? $scope.openCases : 0) + ($scope.inProgressCases !== undefined ? $scope.inProgressCases : 0);
        $scope.closedCases = $scope.closedCases !== undefined ? $scope.closedCases : 0;
        $scope.assignedCases = parseInt($scope.closedCases) + parseInt($scope.openCases) + parseInt($scope.inProgressCases);
        
        $scope.openOrderDetails = function(mon) {
            $rootScope.masterOrderNumber = mon;
            if(window.omniMobile) {
                var mobilePage = "/app/univ/OrderDetail//";  
                $scope.performAction(mobilePage);
            }
            else {
                // $scope.openOrderDetailsPopup();
                $scope.openOrderDetailsPopUpCheck = true;
                $('#DetailModal').addClass('slds-fade-in-open');
                $('#DetailMB-Back').addClass('slds-backdrop_open');
            }
        }
        
        $scope.openOrderDetailsPopup = function() {
            // $('#DetailModal').addClass('slds-fade-in-open');
            // $('#DetailMB-Back').addClass('slds-backdrop_open');
            // $scope.openOrderDetailsPopUpCheck = true;
        }
        
        $scope.closeOrderDetailPopup = function() {
            $('#DetailModal').removeClass('slds-fade-in-open');
            $('#DetailMB-Back').removeClass('slds-backdrop_open');
            $scope.openOrderDetailsPopUpCheck = false;
        }
        
        $scope.goToBillingPage = function() {
            if(window.omniMobile) {
                var mobilePage = "/app/univ/Billing//";
                $scope.performAction(mobilePage);
            }
            else {
                window.parent.location.assign(window.parent.location.href + "current-bills");
            }
        }
        
        $scope.goToTicketsPage = function() {
            if(window.omniMobile) {
                var mobilePage = "/app/univ/Tickets//";
                $scope.performAction(mobilePage);
            }
            else {
                window.parent.location.assign(window.parent.location.href + "casecreation");
            }
        }
        
  }]);